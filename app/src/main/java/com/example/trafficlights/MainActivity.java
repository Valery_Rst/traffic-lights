package com.example.trafficlights;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    private LinearLayout linearLayout;
    private int color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        linearLayout = findViewById(R.id.linearLayout);

        Button redBtn = findViewById(R.id.red_color_btn);
        redBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout.setBackgroundColor(
                        ContextCompat.getColor(MainActivity.this, R.color.colorRed));
                color = getResources().getColor(R.color.colorRed);
            }
        });

        Button yellowColor = findViewById(R.id.yellow_color_btn);
        yellowColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout.setBackgroundColor(
                        ContextCompat.getColor(MainActivity.this, R.color.colorYellow));
                color = getResources().getColor(R.color.colorYellow);
            }
        });

        Button greenBtn = findViewById(R.id.green_color_btn);
        greenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout.setBackgroundColor(
                        ContextCompat.getColor(MainActivity.this, R.color.colorGreen));
                color = getResources().getColor(R.color.colorGreen);
            }
        });
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("color", color);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.containsKey("color")) {
            color = savedInstanceState.getInt("color");
        }

        linearLayout.setBackgroundColor(color);
    }
}